interface Pessoa {
    //... implementar
}

interface Dados {
    pessoas: Pessoa[];
}

let dados: Dados = require('./dados.json');
console.log(dados);
function mediaDeSaldoEmConta(pessoas) {
    //... implementar
    //
}

function mediaIdade(pessoas) {
    //... implementar
    //
}

function getPessoaMaisVelha(pessoa) {
    //... implementar
}

function getQuantidadeDeAmigos(pessoa) {
    //... iml
}

//parte fácil

console.log('Total de Pessoas: ', getTotalDePessoas(dados.pessoas));
console.log('Média do Saldo em Conta', mediaSaldoEmConta(dados.pessoas));
console.log('Média da idade', mediaIdade(dados.pessoas));

const pessoaMaisVelha = getPessoaMaisVelha(dados.pessoas);
console.log(`${pessoaMaisVelha.nome.primeiro} ${pessoaMaisVelha.nome.ultimo} é o mais velho e tem ${pessoaMaisVelha.idade}`);

const pessoaMaisNova = getPessoaMaisNova(dados.pessoas);
console.log(`${pessoaMaisNova.nome.primeiro} ${pessoaMaisNova.nome.ultimo} é o mais novo e tem ${pessoaMaisNova.idade}`);

const maisRico = getPessoaMaisRica(dados.pessoas);
console.log(`${maisRico.nome.primeiro} ${maisRico.nome.ultimo} é o mais rico com T$ ${maisRico.saldoEmConta}`);

const maisPobre = getPessoaMaisPobre(dados.pessoas);
console.log(`${maisPobre.nome.primeiro} ${maisPobre.nome.ultimo} é o mais pobre com T$ ${maisPobre.saldoEmConta}`);



//parte intermediária

interface QuantidadeVivoMorto {
    //implementar
}
const quantVivoMorto: QuantidadeVivoMorto = getQuantidadeVivoMorto(dados.pessoas);
console.log('Pessoas vivas: ', quantVivoMorto.vivo);
console.log('Pessoas mortas: ', quantVivoMorto.morto);

interface CorDosOlhosQuantidade {
    //implementar
    azul;
    castanho;
    verde;
    preto;
}

const corOlhosQuant: CorDosOlhosQuantidade = getCorDosOlhosQuantidade(dados.pessoas);
console.log('Quantidade de ocorrência de Cor dos Olhos: ');
console.log("\t azul: ", corOlhosQuant.azul);
console.log("\t castanho: ", corOlhosQuant.castanho);
console.log("\t verde: ", corOlhosQuant.verde);
console.log("\t preto: ", corOlhosQuant.preto);




//parte desafiadora

//** verificar pelo sobrenome das pessoas */
interface FamiliaQuantidade {
    nomeDaFamilia;
    quantidade;
}
const maiorFamilia: FamiliaQuantidade = getMaiorFamilia(dados.pessoas);
console.log(`Maior família é ${maiorFamilia.nomeDaFamilia}  com ${maiorFamilia.quantidade} membros`);

interface FrutasPreferidasQuantidade {
    'maçã';
    banana;
    pera;
    uva;
    morango;
}

const frutPrefQuant: FrutasPreferidasQuantidade = getFrutasPreferidasQuantidades(dados.pessoas);
console.log('Ocorrências de frutas preferidas: ');
for (let nomeFruta in frutPrefQuant) {
    console.log(nomeFruta, ': ', frutPrefQuant[nomeFruta])
}
